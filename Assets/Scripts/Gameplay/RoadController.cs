using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadController : MonoBehaviour
{
    private const float Delta = 20.0f;
    
    private const int Count = 40;
    private const int BehindCount = 4;
    
    [SerializeField] private Segment m_segmentPrefab;

    // private Vector3 m_lastPosition;
    // private readonly List<Segment> m_segments = new List<Segment>();

    private void Start()
    {
        for (int i = -BehindCount; i < Count - BehindCount; i++)
        {
            Vector3 position = new Vector3(0.0f, 0.0f, i * -Delta);
            Segment segment = Instantiate(m_segmentPrefab, position, Quaternion.identity, transform);
            // segment.OnInvisible += Segment_OnInvisible;
            // m_segments.Add(segment);
            // m_lastPosition = position;
        }
    }

    // private void Segment_OnInvisible(Segment obj)
    // {
    //     obj.transform.position = new Vector3(m_lastPosition.x, m_lastPosition.y, m_lastPosition.z - Delta);
    //     m_lastPosition = obj.transform.position;
    //     obj.IsNeedReshuffle = false;
    // }
}
