using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class CarController : MonoBehaviour
{
    private const string VFXRunPath = "VFX/VFXRun";
    private const string VFXStayPath = "VFX/VFXStay";
    
    private const float MaxSpeed = 100f;
    private const float Delta = 10f;
    private const float Speed = 1000f;
    private const float BrakeSpeed = -5000f;

    private float m_speed = 0.0f;
    private bool m_isMove = false;
    private bool m_isStop = true;

    private float m_path = 0.0f;

    private bool m_isEvent = false;
    public bool IsStart = true;


    [SerializeField] private Animation m_animation;
    [SerializeField] private Rigidbody m_rigidbody;
    [SerializeField] private Transform m_vfxRoot;

    [SerializeField] private WheelCollider m_frontLeftWheelCollider;
    [SerializeField] private WheelCollider m_frontRightWheelCollider;
    [SerializeField] private WheelCollider m_backLeftWheelCollider;
    [SerializeField] private WheelCollider m_backRightWheelCollider;

    [SerializeField] private Transform m_frontLeftWheelTransform;
    [SerializeField] private Transform m_frontRightWheeTransform;
    [SerializeField] private Transform m_backLeftWheelTransform;
    [SerializeField] private Transform m_backRightWheelTransform;

    public float Path => m_path;
    public Rigidbody Rigidbody => m_rigidbody;

    public SceneName m_currentScene;
    public string m_vfxRunPath;
    public string m_vfxStayPath;

    private void Start()
    {
        m_currentScene = LoadManager.CurrentScene;
        m_vfxRunPath = VFXRunPath + m_currentScene;
        m_vfxStayPath = VFXStayPath + m_currentScene;
    }

    private void HandleMotor()
    {
        if (m_rigidbody.velocity.sqrMagnitude < MaxSpeed)
        {
            m_backLeftWheelCollider.motorTorque = m_speed;
            m_backRightWheelCollider.motorTorque = m_speed;
        }
        else
        {
            m_backLeftWheelCollider.motorTorque = 0.0f;
            m_backRightWheelCollider.motorTorque = 0.0f;
        }
    }

    private void UpdateWheels()
    {
        UpdateSingleWheel(m_frontLeftWheelCollider, m_frontLeftWheelTransform);
        UpdateSingleWheel(m_frontRightWheelCollider, m_frontRightWheeTransform);
        UpdateSingleWheel(m_backLeftWheelCollider, m_backLeftWheelTransform);
        UpdateSingleWheel(m_backRightWheelCollider, m_backRightWheelTransform);
    }

    private void UpdateSingleWheel(WheelCollider wheelCollider, Transform wheelTransform)
    {
        wheelCollider.GetWorldPose(out var pos, out var rot);
        wheelTransform.rotation = rot;
        wheelTransform.position = pos;
    }

    public void StartMove()
    {
        m_speed = Speed;
        m_isMove = true;
        m_isStop = false;
        m_backLeftWheelCollider.brakeTorque = 0.0f;
        m_backRightWheelCollider.brakeTorque = 0.0f;
    }


    private GameObject m_vfxGO;
    // private float delayForSound = 0.5f;
    
    private void Update()
    {
        if (m_isEvent || IsStart)
            return;
        
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            StartMove();
            SoundManager.Instance.PlaySFX(SFX.CarStart);
            
            if (m_vfxGO != null)
            {
                Destroy(m_vfxGO);
                m_vfxGO = null;
            }
            m_vfxGO = Instantiate(Resources.Load<GameObject>(m_vfxRunPath), m_vfxRoot);
            m_vfxGO.SetActive(false);
            SoundManager.Instance.PlaySFX(SFX.CarRun, true, 0.0f);
            
            SchedulerService.ScheduleTask(() =>
            {
                if (m_vfxGO != null)
                {
                    m_vfxGO.SetActive(true);
                }

                SoundManager.Instance.SetSFXLoopVolume(1.0f);
            }, 0.6f);
            
        }

        if (Input.GetMouseButtonUp(0))
        {
            m_speed = BrakeSpeed;
            m_isMove = false;
            if (m_vfxGO != null)
            {
                SchedulerService.ScheduleTask(() =>
                {
                    Destroy(m_vfxGO);
                }, 0.5f);
                Instantiate(Resources.Load<GameObject>(m_vfxStayPath), m_vfxRoot);
            }
        }

        if (m_rigidbody.velocity.z > 0.01f && !m_isStop && !m_isMove)
        {
            m_isStop = true;
            m_speed = 0.0f;
            m_backLeftWheelCollider.brakeTorque = float.MaxValue;
            m_backRightWheelCollider.brakeTorque = float.MaxValue;
            SoundManager.Instance.StopSFXLoop(SFX.CarRun);
        }

        if (!m_isStop)
        {
            m_path += m_rigidbody.velocity.sqrMagnitude;
        }

        // if (m_isMove && m_currentScene != SceneName.GameplayCity)
        // {
        //     if (delayForSound > 0.0f)
        //     {
        //         delayForSound -= Time.deltaTime;
        //     }
        //     else
        //     {
        //         delayForSound = 0.5f;
        //         SoundManager.Instance.PlaySFX(SFX.Mud);
        //     }
        // }
    }

    public void Stop()
    { 
        m_isMove = false;
        m_isEvent = true;
        m_isStop = true;
        m_speed = 0.0f;
        m_rigidbody.velocity = Vector3.zero;
        m_backLeftWheelCollider.brakeTorque = float.MaxValue;
        m_backRightWheelCollider.brakeTorque = float.MaxValue;
        m_backLeftWheelCollider.motorTorque = 0.0f;
        m_backRightWheelCollider.motorTorque = 0.0f;

        if (m_vfxGO != null)
        {
            SchedulerService.ScheduleTask(() =>
            {
                Destroy(m_vfxGO);
            }, 0.5f);
            Instantiate(Resources.Load<GameObject>(m_vfxStayPath), m_vfxRoot);
        }
        
        SoundManager.Instance.StopSFXLoop(SFX.CarRun);
        
        // float durationToStop = m_rigidbody.velocity.sqrMagnitude / 30.0f;
        //
        // return durationToStop;
    }
    
    public void Move()
    {
        m_isEvent = false;
    }


    private void FixedUpdate()
    {
        if (m_isEvent || IsStart)
            return;

        HandleMotor();
        UpdateWheels();
    }

    public void PlayAnimation(AnimationClip animationClip)
    {
        m_animation.AddClip(animationClip, animationClip.name);
        m_animation.Play(animationClip.name);
    }
}
