using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Segment : MonoBehaviour
{
    public event Action<Segment> OnInvisible;
    
    [SerializeField] private Renderer[] m_renderers;
    
    public bool IsNeedReshuffle { get; set; }

    // private void Update()
    // {
    //     for (int i = 0; i < m_renderers.Length; i++)
    //     {
    //         if (m_renderers[i].isVisible)
    //         {
    //             return;
    //         }
    //     }
    //
    //     float delta = Camera.main.transform.position.z - transform.position.z;
    //
    //     if (!IsNeedReshuffle && delta < -50.0f)
    //     {
    //         IsNeedReshuffle = true;
    //         OnInvisible?.Invoke(this);
    //     }
    // }
}
