using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Facebook.Unity;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    private const string CarPath = "Cars/";
    private const float Koef = 40000;
    private const float StartValue = 0.086f;
    
    private const float FirstEvent = 0.2f;
    private const float SecondEvent = 0.5f;
    private const float ThirdEvent = 0.8f;
    
    [SerializeField] private RoadController m_roadController;
    [SerializeField] private CarObserver m_carObserver;
    [SerializeField] private Slider m_slider;
    [SerializeField] private Text m_level;
    [SerializeField] private Button m_playButton;
    [SerializeField] private Button m_changeCarButton;

    [SerializeField] private EventContent[] m_eventContent;

    [SerializeField] private GameObject m_root;
    [SerializeField] private ChoiceCard m_firstCard;
    [SerializeField] private ChoiceCard m_secondCard;
    
    [SerializeField] private Transform m_carRoot;
    

    private CarController m_carController;
    private int m_index = 0;
    private float m_progress = 0;
    
    private bool m_isWin;
    private bool m_isLose;
    private bool m_isFirst;
    private bool m_isSecond;
    private bool m_isThird;

    private GameObject m_mainObject;
    private GameObject m_addObject;

    private void OnEnable()
    {
        m_firstCard.Click += FirstCard_OnClick;
        m_secondCard.Click += SecondCard_OnClick;
        m_playButton.onClick.AddListener(PlayButton_OnClick);
        m_changeCarButton.onClick.AddListener(ChangeCarButton_OnClick);
    }
    
    private void OnDisable()
    {
        m_firstCard.Click -= FirstCard_OnClick;
        m_secondCard.Click -= SecondCard_OnClick;
        m_playButton.onClick.RemoveListener(PlayButton_OnClick);
        m_changeCarButton.onClick.RemoveListener(ChangeCarButton_OnClick);
    }

    private void Start()
    {
        m_root.SetActive(false);
        CarType carType = Profile.Instance.GameSettings.CarType;
        CarController carController = Resources.Load<CarController>(CarPath + carType.ToString());
        m_carController = Instantiate(carController , m_carRoot.transform);
        m_carController.name = "Car";
        
        m_carObserver.SetCar(m_carController);
        
        m_slider.value = StartValue;
        m_level.text = Profile.Instance.GameSettings.Level.ToString();

        if (Profile.Instance.GameSettings.IsNeedShowStartPopup)
        {
            m_playButton.gameObject.SetActive(true);
            m_changeCarButton.gameObject.SetActive(true);

            m_carController.IsStart = true;
        }
        else
        {
            m_playButton.gameObject.SetActive(false);
            m_changeCarButton.gameObject.SetActive(false);
            
            m_carController.IsStart = false;
            
            FB.LogAppEvent("user_level_start", null, new Dictionary<string, object>
            {
                { "level", Profile.Instance.GameSettings.Level.ToString() },
                { "level_start_time", DateTime.Now.ToString("G") }
            });
        }
    }

    
    private void PlayButton_OnClick()
    {
        SoundManager.Instance.PlaySFX(SFX.ButtonClick);
        
        m_playButton.gameObject.SetActive(false);
        m_changeCarButton.gameObject.SetActive(false);
        
        if (m_carController != null)
        {
            m_carController.IsStart = false;
            m_carController.StartMove();
        }

        FB.LogAppEvent("user_level_start", null, new Dictionary<string, object>
        {
            { "level", Profile.Instance.GameSettings.Level.ToString() },
            { "level_start_time", DateTime.Now.ToString("G") }
        });
        
       
    }
    
    private void ChangeCarButton_OnClick()
    {
        SoundManager.Instance.PlaySFX(SFX.ButtonClick);
        LoadManager.LoadScene(SceneName.ChangeCar);
    }


    private void Update()
    {
        if (m_isLose)
            return;
        
        m_progress = m_carController.Path / Koef;
        m_slider.value = StartValue + m_progress;
        if (m_progress > FirstEvent && !m_isFirst)
        {
            m_isFirst = true;
            CreateEvent();
        }
        else if (m_progress > SecondEvent && !m_isSecond)
        {
            m_isSecond = true;
            CreateEvent();
        }
        else if (m_progress > ThirdEvent && !m_isThird)
        {
            m_isThird = true;
            CreateEvent();
        }
        else if (m_progress + StartValue > 1.0f && !m_isWin)
        {
            Win();
        }
    }

    private void Win()
    {
        m_isWin = true;
        m_carController.Stop();
        SceneName sceneName = LoadManager.CurrentScene;
        
        if (!Profile.Instance.GameSettings.GamePassed)
        {
            int count = Enum.GetValues(typeof(SceneName)).Length;
            if ((int) sceneName < count)
            {
                sceneName += 1;
            }
            else
            {
                Profile.Instance.GameSettings.GamePassed = true;
            }
        }
        
        if (Profile.Instance.GameSettings.GamePassed)
        {
            List<SceneName> sceneNames = new List<SceneName>();
            sceneNames.Add(SceneName.GameplayCity);
            sceneNames.Add(SceneName.GameplayDesert);
            sceneNames.Add(SceneName.GameplayForest);
            sceneNames.Remove(sceneName);
            sceneName = sceneNames[Random.Range(0, sceneNames.Count)];
        }

        Profile.Instance.GameSettings.Level++;
        Profile.Instance.GameSettings.CurrentLevel = sceneName;
        Profile.Instance.Save();
        
        PopupManager.Instance.Show(PopupType.WinGame);
        
        FB.LogAppEvent("user_level_finish", null, new Dictionary<string, object>
        {
            { "level", Profile.Instance.GameSettings.Level.ToString() },
            { "level_start_time", DateTime.Now.ToString("G") }
        });
    }

    private GameObject m_rootForMainObject;

    private void CreateEvent()
    {
        SoundManager.Instance.StopSFXLoop(SFX.CarRun);
        SoundManager.Instance.PlaySFX(SFX.CarStay, true);
        
        EventContent eventContent = m_eventContent[m_index];

        m_carController.Stop();
        m_rootForMainObject = new GameObject("Root");
        m_rootForMainObject.transform.rotation = Quaternion.identity;
        
        switch (eventContent.EventType)
        {
            case EventType.ForestFirst:
                //грязь
                m_rootForMainObject.transform.position = new Vector3(2.0f, -0.23f, m_carController.gameObject.transform.position.z - 8.0f);
                m_mainObject = Instantiate(eventContent.MainObject, m_rootForMainObject.transform);
                break;
            case EventType.ForestSecond:
                //дерево
                m_rootForMainObject.transform.position = new Vector3(0, 0, m_carController.gameObject.transform.position.z - 5.0f);
                m_rootForMainObject.transform.rotation = Quaternion.Euler(Vector3.back * 90.0f);
                m_mainObject = Instantiate(eventContent.MainObject, m_rootForMainObject.transform);
                break;
            case EventType.ForestThird:
                //мост
                m_rootForMainObject.transform.position = new Vector3(2.0f, -0.23f, m_carController.gameObject.transform.position.z - 8.0f);
                m_mainObject = Instantiate(eventContent.MainObject, m_rootForMainObject.transform);
                break;
            
            
            case EventType.CityFirst:
                //машинка
                m_rootForMainObject.transform.position = new Vector3(1.0f, -0.25f, m_carController.gameObject.transform.position.z - 8.0f);
                m_mainObject = Instantiate(eventContent.MainObject, m_rootForMainObject.transform);
                break;
            case EventType.CitySecond:
                //много машинок
                m_rootForMainObject.transform.position = new Vector3(1.0f, -0.25f, m_carController.gameObject.transform.position.z - 11.0f);
                m_mainObject = Instantiate(eventContent.MainObject, m_rootForMainObject.transform);
                break;
            case EventType.CityThird:
                //здание
                m_rootForMainObject.transform.position = new Vector3(1.0f, -0.23f, m_carController.gameObject.transform.position.z - 8.0f);
                m_rootForMainObject.transform.rotation = Quaternion.Euler(Vector3.up*90.0f);
                m_mainObject = Instantiate(eventContent.MainObject, m_rootForMainObject.transform);
                break;
            
            
            case EventType.DesertFirst:
                //человек
                m_rootForMainObject.transform.position = new Vector3(1.0f, -0.23f, m_carController.gameObject.transform.position.z - 5.0f);
                m_rootForMainObject.transform.rotation = Quaternion.Euler(Vector3.down*90.0f);
                m_mainObject = Instantiate(eventContent.MainObject, m_rootForMainObject.transform);
                break;
            case EventType.DesertSecond:
                //яма
                m_rootForMainObject.transform.position = new Vector3(2.0f, -0.23f, m_carController.gameObject.transform.position.z - 8.0f);
                m_mainObject = Instantiate(eventContent.MainObject, m_rootForMainObject.transform);
                break;
            case EventType.DesertThird:
                m_rootForMainObject.transform.position = Vector3.one;
                m_mainObject = Instantiate(eventContent.MainObject, m_rootForMainObject.transform);
                break;

        }

        m_carObserver.StartEvent(() =>
        {
            StartChoice(eventContent);
        });
    }

    
    private void FirstCard_OnClick()
    {
        SoundManager.Instance.PlaySFX(SFX.ButtonClick);
        SoundManager.Instance.StopSFXLoop(SFX.CarStay);
        
        m_firstCard.StopAnimation(0.0f);
        m_secondCard.StopAnimation();
        
        EventContent eventContent = m_eventContent[m_index];
        m_firstCard.SetChoice(eventContent.IsFirstWin);
        m_secondCard.gameObject.SetActive(false);
        
        Vector3 position = m_carController.transform.position;
        m_carRoot.position = position;
        m_carController.transform.localPosition = Vector3.zero;

        string cardName = string.Empty;

        Vector3 objectPosition = Vector3.zero;
        
        SchedulerService.ScheduleTask(()=>
        {
            switch (eventContent.EventType)
            {
                case EventType.ForestFirst:
                    //крюк
                    
                    m_addObject =  Instantiate(eventContent.FirstObject, m_carRoot.transform);
                    m_carController.PlayAnimation(eventContent.FirstAnimation);
                    m_carObserver.StartEventMove(eventContent.FirstAnimation);

                    cardName = "winch";
                    break;
                case EventType.ForestSecond:
                    //топор
                    m_addObject =  Instantiate(eventContent.FirstObject, m_rootForMainObject.transform);
                    cardName = "axe";
                    break;
                case EventType.ForestThird:
                    //зонт
                    m_addObject =  Instantiate(eventContent.FirstObject, m_carController.transform);
                    m_carController.PlayAnimation(eventContent.FirstAnimation);
                    m_carObserver.StartEventMove(eventContent.FirstAnimation);
                    cardName = "umbrella";
                    break;
                
                
                case EventType.CityFirst:
                    //id card
                    m_mainObject.GetComponent<Animation>().Play(eventContent.FirstAnimation.name);
                    cardName = "idcard";
                    break;
                case EventType.CitySecond:
                    //стоп
                    m_addObject =  Instantiate(eventContent.FirstObject, m_rootForMainObject.transform);
                    m_carController.PlayAnimation(eventContent.FirstAnimation);
                    m_carObserver.StartEventMove(eventContent.FirstAnimation);
                    cardName = "stop";
                    break;
                case EventType.CityThird:
                    //просто ехать
                    m_mainObject.GetComponent<Animation>().Play("City3_House");
                    m_carController.PlayAnimation(eventContent.FirstAnimation);
                    m_carObserver.StartEventMove(eventContent.FirstAnimation);
                    cardName = "forward";
                    break;
                
                
                case EventType.DesertFirst:
                    //гудок
                    m_mainObject.GetComponent<Animation>().Play(eventContent.FirstAnimation.name);
                    cardName = "beep";
                    break;
                case EventType.DesertSecond:
                    //трамплин
                    m_addObject = Instantiate(eventContent.FirstObject, m_carRoot.transform);
                    m_carController.PlayAnimation(eventContent.FirstAnimation);
                    cardName = "trampoline";
                    break;
                case EventType.DesertThird:
                    //бензин
                    Destroy(m_mainObject);
                    cardName = "petrol";
                    break;

            }
            
            m_root.SetActive(false);
            m_firstCard.Reset();
            m_secondCard.Reset();
        }, 1.0f);
        
        
        switch (eventContent.EventType)
        {
            case EventType.ForestFirst:
                //крюк
                objectPosition = m_carRoot.transform.position + eventContent.FirstObject.transform.position;
                break;
            case EventType.ForestSecond:
                //топор
                break;
            case EventType.ForestThird:
                //зонт
                objectPosition = m_carController.transform.position + eventContent.FirstObject.transform.position;
                break;
            
            case EventType.CityFirst:
                //id card
                objectPosition = m_mainObject.transform.position;
                break;
            case EventType.CitySecond:
                //стоп
                objectPosition = m_carController.transform.position;
                break;
            case EventType.CityThird:
                //просто ехать
                objectPosition = m_carController.transform.position;
                break;
            
            case EventType.DesertFirst:
                //гудок
                objectPosition = m_carController.transform.position;
                break;
            case EventType.DesertSecond:
                //трамплин
                break;
            case EventType.DesertThird:
                //бензин
                objectPosition = m_carController.transform.position;
                break;
        }

        if (objectPosition != Vector3.zero)
        {
            Vector2 positionForCard = RectTransformUtility.WorldToScreenPoint(Camera.main, objectPosition);
            m_firstCard.transform.DOMove(positionForCard, 0.5f);
            m_firstCard.transform.DOScale(0.0f, 0.5f).SetDelay(0.5f);
        }

        SchedulerService.ScheduleTask(()=>
        {
            EndEvent(!eventContent.IsFirstWin);
        }, 3.5f);

        FB.LogAppEvent("user_card_choice", null, new Dictionary<string, object>
        {
            { "level", Profile.Instance.GameSettings.Level.ToString() },
            { "card_name", cardName}
        });
    }
    
    
    private void SecondCard_OnClick()
    {
        SoundManager.Instance.PlaySFX(SFX.ButtonClick);
        SoundManager.Instance.StopSFXLoop(SFX.CarStay);
        
        m_firstCard.StopAnimation();
        m_secondCard.StopAnimation();
        
        EventContent eventContent = m_eventContent[m_index];
        m_secondCard.SetChoice(!eventContent.IsFirstWin);
        m_firstCard.gameObject.SetActive(false);
        
        Vector3 position = m_carController.transform.position;
        m_carRoot.position = position;
        m_carController.transform.localPosition = Vector3.zero;
        
        string cardName = string.Empty;
        
        Vector3 objectPosition = Vector3.zero;
        
        SchedulerService.ScheduleTask(()=>
        {
            switch (eventContent.EventType)
            {
                case EventType.ForestFirst:
                    //доски
                    Instantiate(eventContent.SecondObject, m_mainObject.transform);
                    m_carController.PlayAnimation(eventContent.SecondAnimation);
                    cardName = "boards";
                    break;
                case EventType.ForestSecond:
                    //бобёр
                    m_mainObject.GetComponent<Animation>().Play(eventContent.SecondAnimation.name);
                    cardName = "beaver";
                    break;
                case EventType.ForestThird:
                    //доски
                    Instantiate(eventContent.SecondObject, m_mainObject.transform);
                    m_carController.PlayAnimation(eventContent.SecondAnimation);
                    cardName = "boards";
                    break;
                            
                
                case EventType.CityFirst:
                    //таран
                    m_carController.PlayAnimation(eventContent.SecondAnimation);
                    cardName = "forward";
                    break;
                case EventType.CitySecond:
                    //едет много машинок
                    Instantiate(eventContent.SecondObject, m_rootForMainObject.transform);
                    m_carController.PlayAnimation(eventContent.SecondAnimation);
                    cardName = "forward";
                    break;
                case EventType.CityThird:
                    //трамплин
                    Instantiate(eventContent.SecondObject, m_mainObject.transform);
                    m_carController.PlayAnimation(eventContent.SecondAnimation);
                    cardName = "trampoline";
                    break;
                
                
                case EventType.DesertFirst:
                    //сбить человечика
                    m_carController.PlayAnimation(eventContent.SecondAnimation);
                    cardName = "forward";
                    break;
                case EventType.DesertSecond:
                    //доски
                    Instantiate(eventContent.SecondObject, m_mainObject.transform);
                    m_carController.PlayAnimation(eventContent.SecondAnimation);
                    m_carObserver.StartEventMove(eventContent.SecondAnimation);
                    cardName = "boards";
                    break;
                case EventType.DesertThird:
                    //соус
                    m_carController.PlayAnimation(eventContent.SecondAnimation);
                    Destroy(m_mainObject);
                    cardName = "sauce";
                    break;
            }
            
            m_root.SetActive(false);
            m_firstCard.Reset();
            m_secondCard.Reset();
        }, 1.0f);
        
        switch (eventContent.EventType)
        {
            case EventType.ForestFirst:
                //доски
                break;
            case EventType.ForestSecond:
                //бобёр
                objectPosition = m_mainObject.transform.position;
                break;
            case EventType.ForestThird:
                //доски
                break;
            
            case EventType.CityFirst:
                //таран
                break;
            case EventType.CitySecond:
                //едет много машинок
                break;
            case EventType.CityThird:
                //трамплин
                break;

            case EventType.DesertFirst:
                //сбить человечика
                break;
            case EventType.DesertSecond:
                //доски
                objectPosition = m_mainObject.transform.position + eventContent.SecondObject.transform.position;
                break;
            case EventType.DesertThird:
                //соус
                break;

        }
        
        if (objectPosition != Vector3.zero)
        {
            Vector2 positionForCard = RectTransformUtility.WorldToScreenPoint(Camera.main, objectPosition);
            m_secondCard.transform.DOMove(positionForCard, 1.0f);
            m_secondCard.transform.DOScale(0.0f, 1.0f);
        }
        
        SchedulerService.ScheduleTask(()=>
        {
            EndEvent(eventContent.IsFirstWin);
        }, 3.5f);
        
        
        FB.LogAppEvent("user_card_choice", null, new Dictionary<string, object>
        {
            { "level", Profile.Instance.GameSettings.Level.ToString() },
            { "card_name", cardName}
        });
    }
    
    
    private void StartChoice(EventContent eventContent)
    {
        m_root.SetActive(true);
        m_firstCard.SetSprite(eventContent.FirstSprite);
        m_secondCard.SetSprite(eventContent.SecondSprite);
        m_firstCard.StartAnimation();
        SchedulerService.ScheduleTask(() =>
        {
            m_secondCard.StartAnimation();
        }, 2.0f);
        
    }

    private void EndEvent(bool isLose = false)
    {
        if (isLose)
        {
            PopupManager.Instance.Show(PopupType.LoseGame);
        }
        else
        {
            m_carObserver.EndEvent(End);
        }
    }

    private void End()
    {        
        m_carController.Move();
        m_index++;

        if (m_addObject != null)
        {
            Destroy(m_addObject);
            m_addObject = null;
        }

        Vector3 position = m_carController.transform.position;
        m_carRoot.transform.localPosition = Vector3.zero;
        m_carController.transform.localPosition = position;
    }
}
