using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CarObserver : MonoBehaviour
{
    private CarController m_carController;
    private bool m_isInit;
    private bool m_isEventMove;
    private Vector3 m_startPosition = Vector3.zero;
    private Vector3 m_eventStartPosition = Vector3.zero;


    [SerializeField] Animation m_animation;
    [SerializeField] Transform m_root;

    private void Start()
    {
        m_startPosition = transform.position;
        m_isInit = false;
    }

    private void Update()
    {
        if (m_isInit)
        {
            float z = m_startPosition.z + m_carController.transform.position.z;
            transform.position = new Vector3(transform.position.x, transform.position.y, z);
        }
        else if (m_isEventMove)
        {
            float z = m_carController.transform.position.z - m_eventStartPosition.z;
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + z);
        }
    }
    
    public void SetCar(CarController carController)
    {
        m_carController = carController;
        m_isInit = true;
    }

    private Vector3 m_savePosition;
    private Vector3 m_saveRotation;
    
    public void StartEventMove(AnimationClip animationClip)
    {
        m_animation.AddClip(animationClip, animationClip.name);
        m_animation.Play(animationClip.name);
    }
    
    public void StartEvent(Action action)
    {
        m_savePosition = transform.position;
        m_saveRotation = transform.rotation.eulerAngles;

        transform.DORotate(new Vector3(1.145f, 143.71f, -0.789f), 2.0f);
        transform.DOMove(new Vector3(-5.76f, 2.13f, m_savePosition.z + 14.5f), 2.0f).OnComplete(()=>
        {
            action?.Invoke();
        });
        
        m_isInit = false;
    }

    private Action m_action;
    public void EndEvent(Action action)
    {
        m_action = action;
        transform.DORotate(m_saveRotation, 2.0f);
        float z = m_startPosition.z + m_carController.transform.position.z;
        transform.DOMove(new Vector3(m_savePosition.x, m_savePosition.y, z), 2.0f).OnComplete(Action);
    }

    private void Action()
    {
        m_action?.Invoke();
        m_isInit = true;
        
        Vector3 position = transform.position;
        m_root.transform.localPosition = Vector3.zero;
        transform.localPosition = position;
    }
}
