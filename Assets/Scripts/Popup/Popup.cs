using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public abstract class Popup : MonoBehaviour
{
    public abstract PopupType PopupType { get; }

    public event Action<PopupType> PopupClose;
    public event Action<PopupType> PopupShow;

    [SerializeField] private CanvasGroup m_canvasGroup;
    [SerializeField] protected Button m_backButton;

    public void Show()
    {
        gameObject.SetActive(true);

        m_canvasGroup.DOFade(1.0f, 0.5f);
    }
    
    public void Init()
    {
        m_canvasGroup.alpha = 0.0f;
        gameObject.SetActive(false);
        
        if (m_backButton != null)
            m_backButton.onClick.AddListener(Close);
    }


    protected virtual void Close()
    {
        SoundManager.Instance.PlaySFX(SFX.ButtonClick);
        if (m_backButton != null)
            m_backButton.onClick.RemoveListener(Close);

        PopupManager.Instance.PopupCanvas.FadeOut();

        m_canvasGroup.DOFade(0.0f, 0.5f).OnComplete(() =>
        {
            PopupClose?.Invoke(PopupType);
            Destroy(gameObject);
        });
    }

}
