public enum PopupType 
{
    None = 0,
    LoseGame = 1,
    WinGame = 2,
}
