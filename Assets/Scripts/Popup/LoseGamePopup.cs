using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoseGamePopup : Popup
{
    public override PopupType PopupType => PopupType.LoseGame;

    [SerializeField] private Button m_restartButton;

    private void OnEnable()
    {
        m_restartButton.onClick.AddListener(RestartButton_OnClick);
    }

    private void RestartButton_OnClick()
    {
        Profile.Instance.GameSettings.IsNeedShowStartPopup = false;
        SceneName sceneName = LoadManager.CurrentScene;
        LoadManager.LoadScene(sceneName);
        base.Close();
    }

    private void OnDisable()
    {
        m_restartButton.onClick.RemoveListener(RestartButton_OnClick);
    }

    protected override void Close()
    {
        Profile.Instance.GameSettings.IsNeedShowStartPopup = true;
        SceneName sceneName = LoadManager.CurrentScene;
        LoadManager.LoadScene(sceneName);
        base.Close();
    }
}
