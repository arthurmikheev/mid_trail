﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PopupManager : MonoBehaviourSingleton<PopupManager>
{
    private const string PopupPath = "Popup/";
    private const string PopupCanvasPath = "Popup/PopupCanvas";

    public event Action<PopupType> PopupShow = null;
    public event Action<PopupType> PopupClose = null;
    
    private PopupCanvas m_popupCanvas;

    private readonly Queue<Popup> m_popups = new Queue<Popup>();
    private Popup m_currentPopup;
    
    public PopupCanvas PopupCanvas => m_popupCanvas;

    protected override void SingletonAwakened()
    {
        m_popupCanvas = Instantiate(Resources.Load<PopupCanvas>(PopupCanvasPath), transform);
    }

    public void Show(PopupType popupType, bool isImmediately = false)
    {
        Popup popup = Instantiate(Resources.Load<Popup>(PopupPath + popupType + "Popup"), m_popupCanvas.transform);
        popup.Init();
        m_popups.Enqueue(popup);
        ShowPopup();
    }
    private void ShowPopup()
    {
        if (m_currentPopup == null && m_popups.Count > 0)
        {
            m_currentPopup = m_popups.Dequeue();
            m_currentPopup.PopupClose += CurrentPopup_OnPopupClose;
            m_currentPopup.Show();
            m_popupCanvas.FadeIn();
            PopupShow.SafeInvoke(m_currentPopup.PopupType);
        }
    }

    private void CurrentPopup_OnPopupClose(PopupType popupType)
    {
        if (m_currentPopup != null)
        {
            m_currentPopup.PopupShow -= CurrentPopup_OnPopupClose;
            m_currentPopup = null;
        }

        ShowPopup();
        PopupClose?.SafeInvoke(popupType);
    }
}