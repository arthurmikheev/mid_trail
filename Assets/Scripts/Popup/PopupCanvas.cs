﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PopupCanvas : MonoBehaviour
{
    [SerializeField] private Canvas m_canvas;
    [SerializeField] private CanvasScaler m_canvasScaler;
    [SerializeField] private Image m_backImage;
    private void Awake()
    {
	    m_backImage.gameObject.SetActive(false);
    }

    public void FadeIn(bool isImmediately = false)
    {
	    m_backImage.gameObject.SetActive(true);
	    m_backImage.DOFade(0.5f, isImmediately ? 0.0f : 0.5f);
    }

    public void FadeOut(bool isImmediately = false)
    {
	    m_backImage.DOFade(0.0f, isImmediately ? 0.0f : 0.5f).OnComplete(() =>
	    {
		    m_backImage.gameObject.SetActive(false);
	    });

    }
}
