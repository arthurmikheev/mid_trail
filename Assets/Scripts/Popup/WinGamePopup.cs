using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinGamePopup : Popup
{
    public override PopupType PopupType => PopupType.WinGame;

    [SerializeField] private Button m_nextLevelButton;

    private void OnEnable()
    {
        m_nextLevelButton.onClick.AddListener(NextLevelButton_OnClick);
    }

    private void NextLevelButton_OnClick()
    {
        Profile.Instance.GameSettings.IsNeedShowStartPopup = false;
        SceneName sceneName = Profile.Instance.GameSettings.CurrentLevel;
        LoadManager.LoadScene(sceneName);
        
        base.Close();
    }

    private void OnDisable()
    {
        m_nextLevelButton.onClick.RemoveListener(NextLevelButton_OnClick);
    }

    protected override void Close()
    {
        Profile.Instance.GameSettings.IsNeedShowStartPopup = true;
        SceneName sceneName = Profile.Instance.GameSettings.CurrentLevel;
        LoadManager.LoadScene(sceneName);
        
        base.Close();
    }
}
