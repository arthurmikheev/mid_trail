using System.Net.Mime;
using TMGCore;

public class Profile : SingletonManual<Profile>
{
	public GameSettings GameSettings { get; set; }

	private PlayerPrefsImpl m_prefsImpl = null;

	public void Save()
	{
		m_prefsImpl.DeleteAll();
		SaveTo(m_prefsImpl);
	}

	/// <summary>
	/// Save prefs from memory to implementation
	/// </summary>
	public void SaveTo(PlayerPrefsImpl prefsImpl)
	{
		GameSettings.Save(prefsImpl);

		prefsImpl.Save();
	}

	/// <summary>
	/// Load prefs from implementation to memory
	/// </summary>
	public void LoadFrom(PlayerPrefsImpl prefsImpl)
	{
		GameSettings.Load(prefsImpl);
	}

	public void Reset()
	{
		GameSettings = new GameSettings();
	}

	protected override void Init()
	{
		base.Init();

#if UNITY_EDITOR
		m_prefsImpl = new PlayerPrefsEditorImpl();
#else
		m_prefsImpl = new PlayerPrefsUnityObfImpl ();
#endif
		Reset();

		LoadFrom(m_prefsImpl);
	}

	protected override void DeInit()
	{
		base.DeInit();

		GameSettings = null;
	}
}