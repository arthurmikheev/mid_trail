using System;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings
{
    public CarType CarType { get; set; }
    public SceneName CurrentLevel { get; set; }
    
    public bool GamePassed { get; set; }
    public bool MusicEnable { get; set; }
    public bool SoundEnable { get; set; }
    public bool VibrationEnable { get; set; }
    public int SessionCount { get; set; }
    public int Level { get; set; }

    public bool IsFirstSession => SessionCount == 1;


    public bool IsNeedShowStartPopup = true;

    
    private const string CarTypeKey = "CarType";
    
    private const string MusicEnableKey = "MusicEnable";
    private const string SoundEnableKey = "SoundEnable";
    private const string VibrationEnableKey = "VibrationEnable";
    private const string CurrentLevelKey = "CurrentLevel";
    private const string SessionCountKey = "SessionCount";
    private const string GamePassedKey = "GamePassed";
    private const string LevelKey = "Level";

    public void Load(PlayerPrefsImpl prefsImpl)
    {
        MusicEnable = prefsImpl.GetBoolean(MusicEnableKey, true);
        SoundEnable = prefsImpl.GetBoolean(SoundEnableKey, true);
        VibrationEnable = prefsImpl.GetBoolean(VibrationEnableKey, true);
        GamePassed = prefsImpl.GetBoolean(GamePassedKey, false);


        CarType = (CarType)prefsImpl.GetInt32(CarTypeKey, 1);
        CurrentLevel = (SceneName)prefsImpl.GetInt32(CurrentLevelKey, 4);
        Level = prefsImpl.GetInt32(LevelKey, 1);
        
        SessionCount = prefsImpl.GetInt32(SessionCountKey);
    }

    public void Save(PlayerPrefsImpl prefsImpl)
    {
        prefsImpl.SetBoolean(MusicEnableKey, MusicEnable);
        prefsImpl.SetBoolean(SoundEnableKey, SoundEnable);
        prefsImpl.SetBoolean(VibrationEnableKey, VibrationEnable);
        prefsImpl.SetBoolean(GamePassedKey, GamePassed);

        
        prefsImpl.SetInt32(CarTypeKey, (int)CarType);
        prefsImpl.SetInt32(CurrentLevelKey, (int)CurrentLevel);
        prefsImpl.SetInt32(LevelKey, Level);
        
        prefsImpl.SetInt32(SessionCountKey, SessionCount);
        
        
    }
}