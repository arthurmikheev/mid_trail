﻿using System;
using UnityEngine;

public class PlayerPrefsUnityImpl : PlayerPrefsImpl
{
    public override void Save()
    {
        PlayerPrefs.Save();
    }

	public static void DeleteUnityData()
	{
		PlayerPrefs.DeleteAll();
	}

    public override void DeleteAll()
    {
		DeleteUnityData();
	}

	public override bool GetBoolean(string key, bool defaultValue = false)
	{
		return PlayerPrefs.GetInt(key, defaultValue ? 1 : 0) == 1;
	}

	public override void SetBoolean(string key, bool value)
	{
		PlayerPrefs.SetInt(key, value ? 1 : 0);
	}

    public override string GetString(string key, string defaultValue = "")
    {
        return PlayerPrefs.GetString(key, defaultValue);
    }

    public override void SetString(string key, string value)
    {
        PlayerPrefs.SetString(key, value);
    }

    public override int GetInt32(string key, int defaultValue = 0)
    {
        return PlayerPrefs.GetInt(key, defaultValue);
    }

    public override void SetInt32(string key, int value)
    {
        PlayerPrefs.SetInt(key, value);
    }

    public override float GetFloat(string key, float defaultValue = 0)
    {
        return PlayerPrefs.GetFloat(key, defaultValue);
    }

    public override void SetFloat(string key, float value)
    {
        PlayerPrefs.SetFloat(key, value);
    }

    public override Vector3 GetVector3(string key, Vector3 defaultValue = new Vector3())
    {
		return PlayerPrefsX.GetVector3(key, defaultValue);
    }

    public override void SetVector3(string key, Vector3 value)
    {
		PlayerPrefsX.SetVector3(key, value);
    }
}