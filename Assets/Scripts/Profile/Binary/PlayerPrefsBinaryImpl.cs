﻿using System.Collections.Generic;
using System.IO;

public abstract class PlayerPrefsBinaryImpl : PlayerPrefsImpl
{
	private readonly Dictionary<string, bool> m_bools = new Dictionary<string, bool>();
	private readonly Dictionary<string, int> m_ints = new Dictionary<string, int>();
	private readonly Dictionary<string, float> m_floats = new Dictionary<string, float>();
	private readonly Dictionary<string, string> m_strings = new Dictionary<string, string>();

	public override void DeleteAll()
	{
		ClearLists();
	}

	public override bool GetBoolean(string key, bool defaultValue = false)
	{
		bool value;
		if (m_bools.TryGetValue(key, out value))
		{
			return value;
		}
		return defaultValue;
	}

	public override void SetBoolean(string key, bool value)
	{
		m_bools[key] = value;
	}

	public override int GetInt32(string key, int defaultValue = 0)
	{
		int value;
		if (m_ints.TryGetValue(key, out value))
		{
			return value;
		}
		return defaultValue;
	}

	public override void SetInt32(string key, int value)
	{
		m_ints[key] = value;
	}

	public override float GetFloat(string key, float defaultValue = 0)
	{
		float value;
		if (m_floats.TryGetValue(key, out value))
		{
			return value;
		}

		return defaultValue;
	}

	public override void SetFloat(string key, float value)
	{
		m_floats[key] = value;
	}

	public override string GetString(string key, string defaultValue = "")
	{
		string value;
		if (m_strings.TryGetValue(key, out value))
		{
			return value;
		}
		return defaultValue;
	}

	public override void SetString(string key, string value)
	{
		m_strings[key] = value;
	}

	protected void ReadData(Stream stream)
	{
		m_bools.Clear();
		m_ints.Clear();
		m_floats.Clear();
		m_strings.Clear();

		BinaryReader reader = new BinaryReader(stream);
		int boolCount = reader.ReadInt32();
		for (int i = 0; i < boolCount; i++)
		{
			string key = reader.ReadString();
			bool value = reader.ReadBoolean();
			m_bools.Add(key, value);
		}

		int intCount = reader.ReadInt32();
		for (int i = 0; i < intCount; i++)
		{
			string key = reader.ReadString();
			int value = reader.ReadInt32();
			m_ints.Add(key, value);
		}

		int floatCount = reader.ReadInt32();
		for (int i = 0; i < floatCount; i++)
		{
			string key = reader.ReadString();
			float value = reader.ReadSingle();
			m_floats.Add(key, value);
		}

		int strCount = reader.ReadInt32();
		for (int i = 0; i < strCount; i++)
		{
			string key = reader.ReadString();
			string value = reader.ReadString();
			m_strings.Add(key, value);
		}
	}

	protected void WriteData(Stream stream)
	{
		BinaryWriter writer = new BinaryWriter(stream);
		int boolCount = m_bools.Count;
		writer.Write(boolCount);
		foreach (KeyValuePair<string, bool> keyValuePair in m_bools)
		{
			writer.Write(keyValuePair.Key);
			writer.Write(keyValuePair.Value);
		}

		int intCount = m_ints.Count;
		writer.Write(intCount);
		foreach (KeyValuePair<string, int> keyValuePair in m_ints)
		{
			writer.Write(keyValuePair.Key);
			writer.Write(keyValuePair.Value);
		}

		int floatCount = m_floats.Count;
		writer.Write(floatCount);
		foreach (KeyValuePair<string, float> keyValuePair in m_floats)
		{
			writer.Write(keyValuePair.Key);
			writer.Write(keyValuePair.Value);
		}

		int strCount = m_strings.Count;
		writer.Write(strCount);
		foreach (KeyValuePair<string, string> keyValuePair in m_strings)
		{
			writer.Write(keyValuePair.Key);
			writer.Write(keyValuePair.Value);
		}
	}

	protected void ClearLists()
	{
		m_bools.Clear();
		m_ints.Clear();
		m_floats.Clear();
		m_strings.Clear();
	}
}