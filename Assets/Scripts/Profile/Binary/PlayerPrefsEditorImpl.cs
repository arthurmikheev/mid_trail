﻿using System.IO;
using UnityEngine;

public class PlayerPrefsEditorImpl : PlayerPrefsBinaryImpl
{
	private static string FilePath { get { return Path.Combine(Application.persistentDataPath, FileName); } }

	private const string FileName = "EditorPlayerPrefs.bin";
	
	public PlayerPrefsEditorImpl()
	{
		if (File.Exists(FilePath))
		{
			using (FileStream stream = File.OpenRead(FilePath))
			{
				ReadData(stream);
			}
		}
	}

	public override void Save()
	{
		using (FileStream stream = File.Create(FilePath))
		{
			WriteData(stream);
		}
	}

	public static void DeleteEditorData()
	{
		if (File.Exists(FilePath))
		{
			File.Delete(FilePath);
		}
	}

	public override void DeleteAll()
	{
		base.DeleteAll();
		DeleteEditorData();
	}
}