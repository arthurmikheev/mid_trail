﻿using System;
using UnityEngine;

public abstract class PlayerPrefsImpl
{
	public abstract void Save();
	public abstract void DeleteAll();

	public abstract string GetString(string key, string defaultValue = "");
	public abstract void SetString(string key, string value);

    public abstract int GetInt32(string key, int defaulValue = 0);
    public abstract void SetInt32(string key,int value);

    public abstract float GetFloat(string key, float defaultValue = 0);
    public abstract void SetFloat(string key, float value);

    public abstract bool GetBoolean(string key, bool defaultValue = false);
    public abstract void SetBoolean(string key, bool value);

	public DateTime GetDateTime(string key, DateTime defaultDateTime = new DateTime())
	{
		long timeBin = GetInt64(key, defaultDateTime.ToBinary());
		return DateTime.FromBinary(timeBin);
	}

	public void SetDateTime(string key, DateTime value)
	{
		SetInt64(key, value.ToBinary());
	}
	
	public virtual Vector3 GetVector3(string key, Vector3 defaultValue = new Vector3())
	{
		var x = GetFloat(key + "_x", 0);
		var y = GetFloat(key + "_y", 0);
		var z = GetFloat(key + "_z", 0);

		return new Vector3(x, y, z);
	}

	public virtual void SetVector3(string key, Vector3 value)
	{
		SetFloat(key + "_x", value.x);
		SetFloat(key + "_y", value.y);
		SetFloat(key + "_z", value.z);
	}

	public double GetDouble(string key, double defaultValue = 0)
	{
		long defDbLong = BitConverter.DoubleToInt64Bits(defaultValue);
		long dbLong = GetInt64(key, defDbLong);
		return BitConverter.Int64BitsToDouble(dbLong);
	}

	public void SetDouble(string key, double value)
	{
		long dbLong = BitConverter.DoubleToInt64Bits(value);
		SetInt64(key, dbLong);
	}

	protected long GetInt64(string key, long defaultValue)
	{
		int lowBits, highBits;
		SplitLong(defaultValue, out lowBits, out highBits);
		lowBits = GetInt32(key + "_lowBits", lowBits);
		highBits = GetInt32(key + "_highBits", highBits);

		// unsigned, to prevent loss of sign bit.
		ulong ret = (uint)highBits;
		ret = (ret << 32);
		return (long)(ret | (ulong)(uint)lowBits);
	}

	protected void SetInt64(string key, long value)
	{
		int lowBits, highBits;
		SplitLong(value, out lowBits, out highBits);
		SetInt32(key + "_lowBits", lowBits);
		SetInt32(key + "_highBits", highBits);
	}
	
	private static void SplitLong(long input, out int lowBits, out int highBits)
	{
		// unsigned everything, to prevent loss of sign bit.
		lowBits = (int)(uint)(ulong)input;
		highBits = (int)(uint)(input >> 32);
	}
}