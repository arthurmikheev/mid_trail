﻿using System;
using System.IO;
using TMGCore;
using UnityEngine;

public class PlayerPrefsUnityObfImpl : PlayerPrefsBinaryImpl
{
	private const string DataName = "ObfuscatedData";
	private const string RSAPublicKey = "<RSAKeyValue><Modulus>z9BfhNEsB3auC/iTYv3Rgg7P3wOMPw9tqMfUMOEiIaM1O/AiEOIg+dgYzaYm2/RzKam7rKQDX3hvdeqTqhhChD/Q7glMqvEuHN0XKPQw8lw/3+S0j3rK+Bj5OHLnkEdgsV2rUA1NgAtPpzJjEcO/rkkWO/wnHcC9lZkBuSO7iYU=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
	private const string RSAPRivateKey= "<RSAKeyValue><Modulus>z9BfhNEsB3auC/iTYv3Rgg7P3wOMPw9tqMfUMOEiIaM1O/AiEOIg+dgYzaYm2/RzKam7rKQDX3hvdeqTqhhChD/Q7glMqvEuHN0XKPQw8lw/3+S0j3rK+Bj5OHLnkEdgsV2rUA1NgAtPpzJjEcO/rkkWO/wnHcC9lZkBuSO7iYU=</Modulus><Exponent>AQAB</Exponent><P>8XQ9GI90nR/UkVbNUMnGx2Wm1rdOlrSsn7Nskgj6SXuog9y29+FjYrDWHcw2xQxq8OUJhpTcRPrHsyj2CRFlAQ==</P><Q>3FVUNEDBqE/N6hJ3IFVOLBMSUqUw4HY7cZFIq55x1bHexVBHpBe6zaoSIbrxMus7lpNq9VaJReBP1+hrrWIQhQ==</Q><DP>QglMfF8fwalm9akL1kCZQQeoLalMsSMQ40yrMPWLnnVXjVjcIcde6yr6JIQpppLS3LeHd2tXIvaDj5hwUeKUAQ==</DP><DQ>FOsM+YEEYcRh2k0yfiFbMJgG7027nDPdZOTUL2b1i8I9UiJTCuMt2wTAf+9NovXuTFdmmjBDDB1Ft0vbnTRoOQ==</DQ><InverseQ>ckv9cKfEE/BAvadZGgglEJP19ZHAch06dRhELsmojbk6mr+z+bVoTeuteizcQg+QNL/+0NLGjzpV0fZTZAp8Pw==</InverseQ><D>Qhn3qBmYta8MYYZp6zztHT90420PeSeBLAST/BBAeQ6e6jfRvikPcClKim9lQOWalBAHQDzTTdTTL2C0I5Cf5zo6GUdjFwFiDy9uzpbaYeDoJ+vDcSSWD0NEKdAQXO/khmgKXgF8Y7vvyaUaSl2lfMIbW8/wLwIEzy/D2StLmAE=</D></RSAKeyValue>";

	public PlayerPrefsUnityObfImpl()
	{
		string dataString = PlayerPrefs.GetString(DataName, string.Empty);
		if (dataString != string.Empty)
		{
			try
			{
				byte[] obfuscated = Convert.FromBase64String(dataString);
				byte[] array = Crypter.DecryptCombine(obfuscated, RSAPRivateKey);
				using (MemoryStream stream = new MemoryStream(array))
				{
					ReadData(stream);
				}
			}
			catch (Exception ex)
			{
				TMGCore.Logger.LogError(ex.ToString());
				ClearLists();
				PlayerPrefs.DeleteKey(DataName);
			}
		}
	}

	public override void DeleteAll()
	{
		base.DeleteAll();
		PlayerPrefs.DeleteKey(DataName);
	}

	public override void Save()
	{
		using (MemoryStream stream = new MemoryStream())
		{
			WriteData(stream);
			byte[] array = stream.ToArray();
			byte[] obfuscated = Crypter.EncryptCombine(array, RSAPublicKey);
			string dataString = Convert.ToBase64String(obfuscated);
			PlayerPrefs.SetString(DataName, dataString);
		}
	}
}