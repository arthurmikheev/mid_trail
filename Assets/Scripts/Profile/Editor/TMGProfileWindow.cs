﻿using UnityEditor;
using UnityEngine;

public class TMGProfileWindow : EditorWindow
{
	[MenuItem("Window/TMG Profile Window")]
	private static void Init()
	{
		// Get existing open window or if none, make a new one:
		TMGProfileWindow window = (TMGProfileWindow)GetWindow(typeof(TMGProfileWindow));
		window.Show();
	}

	protected void OnGUI()
	{
		if (GUILayout.Button("Clear editor profile"))
		{
			PlayerPrefsEditorImpl.DeleteEditorData();
		}

		if (GUILayout.Button("Clear unity profile"))
		{
			PlayerPrefsUnityImpl.DeleteUnityData();
		}
	}
}