﻿using System;
using Facebook.Unity;
using UnityEngine;

public class FacebookInitialization : MonoBehaviour
{
    private void Start()
    {
        Debug.Log("START Facebook Init");
        Initialize();
    }

    private void Initialize()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(InitCallback);
        }
        else
        {
            Debug.Log("Facebook Init Success");
            FB.ActivateApp();
            if (Profile.Instance.GameSettings.IsFirstSession)
            {
                FB.LogAppEvent("user_start");
            }
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            Debug.Log("Facebook Init Success");
            FB.ActivateApp();
            if (Profile.Instance.GameSettings.IsFirstSession)
            {
                FB.LogAppEvent("user_start");
            }
        }
    }
}