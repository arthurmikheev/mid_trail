﻿using UnityEngine;

public class SoundManager : MonoBehaviourSingleton<SoundManager>
{
    [SerializeField] AudioSource audioSourceSFX = null;
    [SerializeField] AudioSource audioSourceSFXForLoop = null;
    [SerializeField] AudioSource audioSourceBGM = null;

    [SerializeField] SoundSource soundSourceSFX = null;
    [SerializeField] SoundSource soundSourceBGM = null;

    
    
    protected override void SingletonStarted()
    {
        UpdateMute();
    }

    public void PlaySFX(string soundName, bool isLoop = false, float volume = -1.0f)
    {
        AudioClip audioClip = soundSourceSFX.GetSound(soundName);
        if (audioClip != null)
        {
            if (isLoop)
            {
                if (!(volume < 0.0f))
                {
                    audioSourceSFXForLoop.volume = volume;
                }
                audioSourceSFXForLoop.loop = true;
                audioSourceSFXForLoop.clip = audioClip;
                audioSourceSFXForLoop.Play();
            }
            else
            {
                if (volume < 0.0f)
                {
                    audioSourceSFX.PlayOneShot(audioClip);
                }
                else
                {
                    audioSourceSFX.PlayOneShot(audioClip, volume);
                }
            }
        }
    }

    public void SetSFXLoopVolume(float volume)
    {
        audioSourceSFXForLoop.volume = volume;
    }

    public void StopSFXLoop(string soundName)
    {
        AudioClip audioClip = soundSourceSFX.GetSound(soundName);
        if (audioSourceSFXForLoop.clip == audioClip)
        {
            audioSourceSFXForLoop.Pause();
            audioSourceSFXForLoop.clip = null;
            audioSourceSFXForLoop.loop = false;
        }
    }

    public void PlayBGM(string soundName)
    {
        AudioClip audioClip = soundSourceBGM.GetSound(soundName);

        if (audioClip == null || (audioSourceBGM.clip != null && audioSourceBGM.clip == audioClip))
            return;

        audioSourceBGM.clip = audioClip;
        audioSourceBGM.loop = true;
        audioSourceBGM.Play();
    }


    public void UpdateMute()
    {
        audioSourceBGM.mute = !Profile.Instance.GameSettings.MusicEnable;
        audioSourceSFX.mute = !Profile.Instance.GameSettings.SoundEnable;
    }
}
