﻿public static class BGM
{
    public const string Main = "Main";
    public const string MiniGame = "MiniGame";
}


public static class SFX
{
    public const string CarRun = "CarRun";
    public const string CarStart = "CarStart";
    public const string ButtonClick = "ButtonClick";
    public const string CarStay = "CarStay";
    public const string Mud = "Mud";
}
