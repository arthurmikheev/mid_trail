using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuCanvas : MonoBehaviour
{
    [SerializeField] private Button m_playButton;
    [SerializeField] private Button m_changeCarButton;

    private void OnEnable()
    {
        m_playButton.onClick.AddListener(PlayButton_OnClick);
        m_changeCarButton.onClick.AddListener(ChangeCarButton_OnClick);
    }

    private void OnDisable()
    {
        m_playButton.onClick.RemoveListener(PlayButton_OnClick);
        m_changeCarButton.onClick.RemoveListener(ChangeCarButton_OnClick);
    }

    private void PlayButton_OnClick()
    {
        SceneName sceneName = Profile.Instance.GameSettings.CurrentLevel;
        LoadManager.LoadScene(sceneName);
    }
    
    private void ChangeCarButton_OnClick()
    {
        LoadManager.LoadScene(SceneName.ChangeCar);
    }

}
