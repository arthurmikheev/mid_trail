using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;
using UnityEngine.UI;
public class ChangeCarCanvas : MonoBehaviour
{
    private const string CarPath = "Cars/";
    
    [SerializeField] private Button m_backButton;
    [SerializeField] private Transform m_root;

    private CarController m_carController;

    private void OnEnable()
    {
        FB.LogAppEvent("car_menu");
        
        m_backButton.onClick.AddListener(BackButton_OnClick);
        CarCard.Click += CarCard_OnClick;
    }

    private void Start()
    {
        CarController carController = Resources.Load<CarController>(CarPath + Profile.Instance.GameSettings.CarType.ToString());
        m_carController = Instantiate(carController, new Vector3(-0.46f, 0.37f, -0.31f), Quaternion.Euler(Vector3.up * 142.5f),  m_root.transform);
    }

    private void OnDisable()
    {
        m_backButton.onClick.RemoveListener(BackButton_OnClick);
        CarCard.Click -= CarCard_OnClick;
    }

    private void BackButton_OnClick()
    {
        Profile.Instance.GameSettings.IsNeedShowStartPopup = true;
        SceneName sceneName = Profile.Instance.GameSettings.CurrentLevel;
        LoadManager.LoadScene(sceneName);
    }
    
    
    private void CarCard_OnClick(CarType carType)
    {
        if (m_carController != null)
        {
            Destroy(m_carController.gameObject);
        }
        
        CarController carController = Resources.Load<CarController>(CarPath + carType.ToString());
        m_carController = Instantiate(carController, new Vector3(-0.46f, 0.37f, -0.31f), Quaternion.Euler(Vector3.up * 142.5f),  m_root.transform);

        Profile.Instance.GameSettings.CarType = carType;
        Profile.Instance.Save();
    }
}
