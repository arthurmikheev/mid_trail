using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarCard : MonoBehaviour
{
    public static event Action<CarType> Click;
    
    [SerializeField] private CarType m_carType;
    [SerializeField] private Button m_button;

    public CarType CarType => m_carType;


    private void OnEnable()
    {
        m_button.onClick.AddListener(Button_OnClick);
    }
    
    private void OnDisable()
    {
        m_button.onClick.RemoveListener(Button_OnClick);
    }

    private void Button_OnClick()
    {
        Click?.Invoke(m_carType);
    }
}
