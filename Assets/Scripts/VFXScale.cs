using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class VFXScale : MonoBehaviour
{
   [SerializeField] private Transform[] m_transform;

   private void Start()
   {
      for (int i = 0; i < m_transform.Length; i++)
      {
         m_transform[i].localScale = Vector3.zero;
         m_transform[i].DOScale(1.0f, 1.0f).SetDelay(0.6f);
      }
   }
}
