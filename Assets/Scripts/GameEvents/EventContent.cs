using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EventContent")]
public class EventContent : ScriptableObject
{
    [SerializeField] private EventType m_eventType;
    [SerializeField] private GameObject m_mainObject;
    [SerializeField] private GameObject m_firstObject;
    [SerializeField] private GameObject m_secondObject;
    [SerializeField] private Sprite m_firstSprite;
    [SerializeField] private Sprite m_secondSprite;
    [SerializeField] private AnimationClip m_firstAnimation;
    [SerializeField] private AnimationClip m_secondAnimation;
    
    [SerializeField] private bool m_isFirstWin;
    
    public EventType EventType => m_eventType;
    public GameObject MainObject => m_mainObject;
    public GameObject FirstObject => m_firstObject;
    public AnimationClip SecondAnimation => m_secondAnimation;
    public AnimationClip FirstAnimation => m_firstAnimation;
    public GameObject SecondObject => m_secondObject;
    public Sprite FirstSprite => m_firstSprite;
    public Sprite SecondSprite => m_secondSprite;
    public bool IsFirstWin => m_isFirstWin;
}

    

    
