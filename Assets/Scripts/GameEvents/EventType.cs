using System;

public enum EventType 
{
    None = 0,
    ForestFirst = 1,
    ForestSecond = 2,
    ForestThird = 3,
    
    DesertFirst = 100,
    DesertSecond = 101,
    DesertThird = 102,
    
    CityFirst = 200,
    CitySecond = 201,
    CityThird = 202
    
}


public static class EventTypeExtentions
{
    public static EventType ToEventType(this SceneName sceneName, int index)
    {
        switch (sceneName)
        {
            case SceneName.GameplayForest:
                switch (index)
                {      
                    case 0:
                        return EventType.ForestFirst;
                    case 1:
                        return EventType.ForestSecond;
                    case 2:
                        return EventType.ForestThird;
                }

                break;

            case SceneName.GameplayDesert:
                switch (index)
                {      
                    case 0:
                        return EventType.DesertFirst;
                    case 1:
                        return EventType.DesertSecond;
                    case 2:
                        return EventType.DesertThird;
                }

                break;
            case SceneName.GameplayCity:
                switch (index)
                {      
                    case 0:
                        return EventType.CityFirst;
                    case 1:
                        return EventType.CitySecond;
                    case 2:
                        return EventType.CityThird;
                }

                break;
        }

        return EventType.None;
    }
}
