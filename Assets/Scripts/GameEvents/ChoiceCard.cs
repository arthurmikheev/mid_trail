using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ChoiceCard : MonoBehaviour
{
    public event Action Click;
    
    [SerializeField] private Image m_image;
    [SerializeField] private Button m_button;
    [SerializeField] private GameObject m_winChoice;
    [SerializeField] private GameObject m_looseChoice;

    private Sequence sequence;

    private Vector3 m_startPosition;
    public void StopAnimation(float duration = 1.0f)
    {
        sequence.Kill();
        transform.DOScale(1.0f, duration);
    }
    
    public void StartAnimation()
    {
        sequence = DOTween.Sequence();
        sequence.AppendInterval(2.0f).Append(transform.DOScale(0.9f, 1.0f)).Append(transform.DOScale(1.0f, 1.0f)).SetLoops(-1,LoopType.Restart);
        //transform.DOScale(0.9f, 1.0f).SetLoops(-1, LoopType.Yoyo).SetDelay(delay);
    }
    
    private void OnEnable()
    {
        m_button.onClick.AddListener(Button_OnClick);
    }
    
    private void OnDisable()
    {
        m_button.onClick.RemoveListener(Button_OnClick);
    }

    private void Start()
    {
        m_startPosition = transform.position;
        m_winChoice.SetActive(false);
        m_looseChoice.SetActive(false);
    }

    public void SetSprite(Sprite sprite)
    {
        m_image.sprite = sprite;
    }

    public void SetChoice(bool isWin)
    {
        if (isWin)
        {
            m_winChoice.SetActive(true);
        }
        else
        {
            m_looseChoice.SetActive(true);
        }

        m_button.enabled = false;
    }
    
    public void Reset()
    {
        sequence.Kill();
        gameObject.SetActive(true);
        m_winChoice.SetActive(false);
        m_looseChoice.SetActive(false);
        m_button.enabled = true;
        transform.localScale = Vector3.one;
        transform.position = m_startPosition;
    }

    private void Button_OnClick()
    {
        m_button.enabled = false;
        Click?.Invoke();
    }
}
