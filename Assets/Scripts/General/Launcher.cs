﻿using System;
using Facebook.Unity;
using TMGCore;
using UnityEngine;
using UnityEngine.UI;

public class Launcher : MonoBehaviour
{
    public static event Action<bool> EventApplicationPaused;
    public static event Action EventApplicationQuit;

    private static bool m_isInit = false;
    
    protected virtual void Awake()
    {
        if (m_isInit)
        {
            Destroy(this);
        }
        else
        {
            Initialize();
        }
    }

    private void OnApplicationPause(bool isPause)
    {
        if (m_isInit)
        {
            Profile.Instance.Save();
            
            EventApplicationPaused?.Invoke(isPause);
        }
    }

    private void OnApplicationQuit()
    {
        if (m_isInit)
        {
            Profile.Instance.Save();

            EventApplicationQuit?.Invoke();
        }
    }

    private void Initialize()
    {
        Application.targetFrameRate = 60;
        Crypter.Instance = new NetCrypter();
        Profile.Instance = new Profile();
        Vibration.Init();
        LoadManager.Init();
        PopupManager popupManager = PopupManager.Instance;

        DontDestroyOnLoad(gameObject);
        m_isInit = true;
        
        Profile.Instance.GameSettings.IsNeedShowStartPopup = true;
        LoadManager.LoadScene(Profile.Instance.GameSettings.CurrentLevel);

        Profile.Instance.GameSettings.SessionCount++;
    }
}