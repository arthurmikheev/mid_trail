﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LoadServiceScenes : LoadService
{
    [Header("Loading UI")]
    [SerializeField]
    private Slider m_LoadingProgress;

    protected override void StartLoadTween()
    {
        m_LoadingProgress.DOValue(1.0f, m_loadSpeed / 2).OnComplete(() =>
        {
            EventInvoke();
        });
    }
}