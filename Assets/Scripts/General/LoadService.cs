﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadService : MonoBehaviour
{
    public static event Action ComleteProgress;
    
    protected float m_loadSpeed;


    protected virtual void Start()
    {
        m_loadSpeed = LoadManager.LoadSpeed;
        
        LoadManager.LoadSceneAsync();
        StartLoadTween();
    }

    protected void EventInvoke()
    {
        ComleteProgress?.Invoke();
    }
    
    protected virtual void StartLoadTween()
    {
        Sequence sequence = DOTween.Sequence();

        sequence.AppendInterval(m_loadSpeed / 2).AppendCallback(() =>
        {
            EventInvoke();
        });
    }
}
