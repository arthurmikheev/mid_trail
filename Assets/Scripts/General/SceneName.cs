public enum SceneName
{
    None = 0,
    
    //Loaders
    SceneLoader = 1,
    
    ChangeCar = 3,
    GameplayForest = 4,
    GameplayCity = 5,
    GameplayDesert = 6,
}
