﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class ExtensionMethods
{
    public static Color SetOpacity(this Color color, float a)
    {
        Color newColor = new Color(color.r, color.g, color.b, a);
        return newColor;
    }
    
    public static void SetOpacity(this Image image, float a)
    {
        image.color = image.color.SetOpacity(a);
    }
    
    public static void SetOpacity(this Text text, float a)
    {
        text.color = text.color.SetOpacity(a);
    }
}
