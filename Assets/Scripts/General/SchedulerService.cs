﻿using System;
using System.Collections;
using UnityEngine;

public class SchedulerService 
{
    public static void ScheduleTask(Action task, float secondsDelay)
    {
        if (secondsDelay <= 0.0f)
        {
            task?.Invoke();
        }
        else
        {
            GameObject gameObject = new GameObject("SchedulerCoroutine");
            SchedulerCoroutine schedulerCoroutine = gameObject.AddComponent<SchedulerCoroutine>();
            schedulerCoroutine.StartCoroutine(task, secondsDelay);
        }
    }
}

public class SchedulerCoroutine : MonoBehaviour
{
    private Coroutine m_coroutine;


    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void StartCoroutine(Action task, float secondsDelay)
    {
        if (m_coroutine == null)
        {
            m_coroutine = StartCoroutine(DelayCoroutine(task, secondsDelay));
        }
    }
    
    public void StopCoroutine()
    {
        if (m_coroutine != null)
        {
            StopCoroutine(m_coroutine);
        }
    }
    
    private IEnumerator DelayCoroutine(Action task, float secondsDelay)
    {
        yield return  new WaitForSeconds(secondsDelay);
        task?.Invoke();
        Destroy(gameObject);
    }
    
}
