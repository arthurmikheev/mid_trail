﻿using System;
using UnityEngine.SceneManagement;

public static class SceneNameEnumExtentionss
{
    public static bool IsLoadScene(this SceneName scene) 
    {
        if (scene == SceneName.SceneLoader)
        {
            return true;
        }
        return false;
    }
    public static SceneName ConverToScene(this Scene scene)
    {
        SceneName returnScene = SceneName.None;
        Enum.TryParse(scene.name, true, out returnScene);
        return returnScene;
    }
}

