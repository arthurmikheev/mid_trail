using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class LoadManager
{
    public static event Action<SceneName> EventSceneUnLoaded;
    public static event Action<SceneName> EventSceneLoaded;
    private static SceneName m_mainScene;

    private static SceneName m_currentScene;
    private static SceneName m_previousScene;
    private static SceneName m_nextScene = SceneName.None;
    private static float m_loadSpeed = 0;

    private static AsyncOperation AsyncLoadScene = null;

    public static SceneName CurrentScene
    {
        get { return m_currentScene; }
    }

    public static SceneName PreviousScene
    {
        get { return m_previousScene; }
    }


    public static SceneName NextScene
    {
        get { return m_nextScene; }
    }

    public static float LoadSpeed
    {
        get { return m_loadSpeed; }
    }

    public static void Init()
    {
        m_mainScene = SceneName.GameplayForest;
        m_nextScene = SceneName.None;
        m_currentScene = SceneName.None;
        m_previousScene = SceneName.None;

        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.sceneUnloaded += OnsSceneUnloaded;
    }

    private static void OnSceneLoaded(Scene scene, LoadSceneMode loadMode)
    {
        m_currentScene = scene.ConverToScene();

        if (!m_currentScene.IsLoadScene())
        {
            EventSceneLoaded?.Invoke(m_currentScene);
        }
    }

    private static void OnsSceneUnloaded(Scene scene)
    {
        if (!scene.ConverToScene().IsLoadScene())
        {
            m_previousScene = scene.ConverToScene();
            EventSceneUnLoaded?.Invoke(m_previousScene);
        }
    }

    private static void LoadScene(SceneName sceneName)
    {
        SceneManager.LoadScene(sceneName.ToString());
    }

    public static void LoadPreviousScene()
    {
        LoadScene(m_previousScene);
    }

    public static void LoadPreviousScene(float loadSpeed = 0.0f)
    {
        m_nextScene = m_previousScene;
        m_loadSpeed = loadSpeed;

        LoadSceneFromLoader();
    }

    public static void ReLoadCurrentScene(float loadSpeed = 0f)
    {
        m_nextScene = m_currentScene;
        m_loadSpeed = loadSpeed;

        LoadSceneFromLoader();
    }

    public static void LoadScene(SceneName sceneName, float loadSpeed = 3.0f)
    {
        m_nextScene = sceneName;
        m_loadSpeed = loadSpeed;

        LoadSceneFromLoader();
    }

    private static void LoadSceneFromLoader(SceneName loaderScene = SceneName.SceneLoader)
    {
        if (m_nextScene == SceneName.None)
            m_nextScene = m_mainScene;

#if UNITY_EDITOR
        LoadScene(m_nextScene);
        return;
#endif
        
        if (m_loadSpeed <= 0.0f)
        {
            LoadScene(m_nextScene);
            return;
        }

        LoadService.ComleteProgress += OnComleteProgress;
        SceneManager.LoadScene(loaderScene.ToString(), LoadSceneMode.Single);
    }

    private static void OnComleteProgress()
    {
        if (AsyncLoadScene != null)
            AsyncLoadScene.allowSceneActivation = true;

        AsyncLoadScene = null;
    }

    public static void LoadSceneAsync()
    {
        AsyncLoadScene = SceneManager.LoadSceneAsync(m_nextScene.ToString());

        if (AsyncLoadScene == null)
        {
            LoadScene(m_mainScene);
            return;
        }

        AsyncLoadScene.allowSceneActivation = false;
    }
}