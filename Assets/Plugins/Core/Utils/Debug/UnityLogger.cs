﻿using UnityEngine;

using Logger = TMGCore.Logger;

public class UnityLogger : Logger
{
	protected override void LogMsg(string message)
	{
		Debug.Log(message);
	}

	protected override void LogWarningMsg(string message)
	{
		Debug.LogWarning(message);
	}

	protected override void LogErrorMsg(string message)
	{
		Debug.LogError(message);
	}
}