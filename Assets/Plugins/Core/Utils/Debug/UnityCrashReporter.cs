﻿using System;
using System.IO;
using TMGCore;
using Application = UnityEngine.Application;
using Debug = UnityEngine.Debug;
using LogType = UnityEngine.LogType;

public class UnityCrashReporter : CrashReporter
{
	protected override void Init()
	{
		base.Init();

		ProcessCrashReports();
		
		Application.logMessageReceived += OnApplicationLog;
		AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
	}

	protected override void DeInit()
	{
		base.DeInit();

		AppDomain.CurrentDomain.UnhandledException -= OnUnhandledException;
		Application.logMessageReceived -= OnApplicationLog;
	}

	protected virtual void OnProcessCrashReport(string file, string version, string header, string stacktrace)
	{
	}

	protected void OnFileProcessed(string file)
	{
		string fileName = Path.GetFileName(file);
		string newFile = string.Format("{0}/{1}{2}", CrashReportDirectoryName, CrashReportPrecessedPrefix, fileName);
		File.Move(file, newFile);
	}

	private void ProcessCrashReports()
	{
		try
		{
			string pattern = string.Format("{0}*{1}", CrashReportPrefix, CrashReportExtention);
			string[] files = Directory.GetFiles(CrashReportDirectoryName, pattern);
			foreach (string file in files)
			{
				string gameVersion = string.Empty;
				string header;
				string stacktrace;
				using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
				{
					using (StreamReader reader = new StreamReader(stream))
					{
						int version;
						string versionStr = reader.ReadLine();
						if(int.TryParse(versionStr, out version))
						{
							gameVersion = reader.ReadLine();
							header = reader.ReadLine();
						}
						else
						{
							gameVersion = "2.0.2";
							header = reader.ReadLine();
						}

						stacktrace = reader.ReadToEnd();
					}
				}

				OnProcessCrashReport(file, gameVersion, header, stacktrace);
			}
		}
		catch
		{
			// ignored
		}
	}

	private void OnApplicationLog(string condition, string stackTrace, LogType type)
	{
		if (type != LogType.Exception)
		{
			return;
		}

	    if (condition.Contains("TypeLoadException: Could not load type 'UnityEngine.Analytics.AnalyticsEvent'"))
	    {
	        Debug.Log("[IGNOR ERROR]" + condition);
            return;
        }
        
        using (DdMonitor.Lock(m_sync))
		{
			if (m_shutingDown)
			{
				return;
			}

			m_shutingDown = true;
#if !UNITY_EDITOR || ENABLE_EDITOR_CRASH_REPORT
			var crashReportFileName = CrashReportFileName;
			using (StreamWriter strWriter = File.CreateText(crashReportFileName))
			{
				strWriter.WriteLine(Version.ToString());
				strWriter.WriteLine(GameVersion);
				strWriter.WriteLine(condition);
				strWriter.Write(stackTrace);
			}
#endif

#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
			MainThread.Call(() => UnityEditor.EditorApplication.isPlaying = false);
#elif UNITY_STANDALONE
			System.Diagnostics.Process.GetCurrentProcess().Kill();
#else
			Application.Quit();
#endif
		}
	}

	private void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
	{
		Exception ex = (Exception)e.ExceptionObject;
				OnApplicationLog(ex.Message, ex.StackTrace, LogType.Exception);
	}

	private static string CrashReportName
	{
		get { return CrashReportPrefix + DateTime.UtcNow.ToString("yyyy-MM-dd HH.mm.ss") + CrashReportExtention; }
	}

	private static string CrashReportDirectoryName
	{
		get
		{
#if UNITY_EDITOR
			DirectoryInfo prev = Directory.GetParent(Application.dataPath);
			return prev.FullName;
#elif UNITY_STANDALONE
			DirectoryInfo prev = Directory.GetParent(Application.dataPath);
			return prev.FullName;
#else
			return Application.persistentDataPath;
#endif
		}
	}

	private static string CrashReportFileName
	{
		get
		{
			return Path.Combine(CrashReportDirectoryName, CrashReportName);
		}
	}

	protected virtual string GameVersion { get { return string.Empty; } }

	protected const string CrashReportPrecessedPrefix = "sent_";
	protected const string CrashReportPrefix = "crash_report_";
	protected const string CrashReportExtention = ".txt";

	private const int Version = 1;
		
	private static bool m_shutingDown = false;
}