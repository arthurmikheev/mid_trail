﻿//using System.Threading;
using TMGCore;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class UnityTicker : Ticker
{
	protected override void Init()
	{
		base.Init();
/*
#if UNITY_EDITOR
		EditorApplication.playmodeStateChanged += OnPlaymodeStateChanged;
#endif
*/
//#if UNITY_WEBGL
		m_enumerator = Tick();
		UnityMainThread.CallCoroutine(m_enumerator);
		/*
#else
		// NOTE: this thread continue working after stop play mode in Unity
		m_thread = new Thread(Tick);
		m_thread.Start();
#endif
*/
	}

	protected override void DeInit()
	{
		base.DeInit();

		CancelTicking();
	}

//#if UNITY_WEBGL
	protected System.Collections.IEnumerator Tick()
		/*
#else
	protected void Tick()
#endif
*/
	{
		while (true)
		{
//#if UNITY_WEBGL
			yield return new UnityEngine.WaitForEndOfFrame();
			/*
#else
			Thread.Sleep(1);
#endif
*/
			TickInternal();
		}
	}

	private void CancelTicking()
	{
//#if UNITY_WEBGL
		UnityMainThread.CancelCoroutine(m_enumerator);
		/*
#else
		if (m_thread != null)
		{
			m_thread.Abort();
			m_thread = null;
		}
#endif
*/
	}
	/*
#if UNITY_EDITOR
	private void OnPlaymodeStateChanged()
	{
		if (EditorApplication.isPlaying)
		{
			return;
		}
		
		EditorApplication.playmodeStateChanged -= OnPlaymodeStateChanged;
		/*
#if !UNITY_WEBGL
		if(m_thread != null)
		{
			m_thread.Abort();
			m_thread = null;
		}
#endif
*//*
	}
#endif
*/
	//#if UNITY_WEBGL
	private System.Collections.IEnumerator m_enumerator = null;
	/*
#else
	private Thread m_thread = null;
#endif
*/
}