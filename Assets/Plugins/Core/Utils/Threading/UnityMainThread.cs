﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TMGCore;
using UnityEngine;

public class UnityMainThread : MainThread
{
	public UnityMainThread(TMGCore.Logger logger)
	{
	}

	protected override void Init()
	{
		base.Init();

		s_unityMainThread = this;

		GameObject mainThread = new GameObject((typeof(UnityMainThread)).ToString());
		Object.DontDestroyOnLoad(mainThread);
		m_monoBehaviour = mainThread.AddComponent<UnityMainThreadBehaviour>();
		m_monoBehaviour.StartCoroutine(Executer());
	}
	
	public static void CallCoroutine(IEnumerator enumerator)
	{
		Instance.CallCoroutineInternal(enumerator);
	}

	public static void CancelCoroutine(IEnumerator enumerator)
	{
		Instance.CancelCoroutineInternal(enumerator);
	}

	private void CallCoroutineInternal(IEnumerator enumerator)
	{
		Assert.TestStrong(m_mainThreadId != 0, "Main thread wasn't created");
		if (m_mainThreadId == Thread.CurrentThread.ManagedThreadId)
		{
			m_monoBehaviour.StartCoroutine(enumerator);
			return;
		}

		lock (s_enumerators)
		{
			s_enumerators.Enqueue(enumerator);
		}
	}

	private void CancelCoroutineInternal(IEnumerator enumerator)
	{
		Assert.TestStrong(m_mainThreadId != 0, "Main thread wasn't created");
		if (m_mainThreadId == Thread.CurrentThread.ManagedThreadId)
		{
			m_monoBehaviour.StopCoroutine(enumerator);
			return;
		}

		lock (s_stopEnumerators)
		{
			s_stopEnumerators.Enqueue(enumerator);
		}
	}

	private IEnumerator Executer()
	{
		while (true)
		{
			yield return new WaitForEndOfFrame();
			
			TickInternal();

			while (s_enumerators.Count > 0)
			{
				m_monoBehaviour.StartCoroutine(s_enumerators.Peek());
				lock (s_enumerators)
				{
					s_enumerators.Dequeue();
				}
			}

			while (s_stopEnumerators.Count > 0)
			{
				m_monoBehaviour.StopCoroutine(s_stopEnumerators.Peek());
				lock (s_stopEnumerators)
				{
					s_stopEnumerators.Dequeue();
				}
			}
		}
	}

	/*public static bool IsDestroyed
	{
		get { return Instance.IsDestroyed; }
	}*/

	public new static UnityMainThread Instance
	{
		get { return s_unityMainThread; }
	}

	public static bool IsDestroyed
	{
		get { return Instance.m_monoBehaviour == null; }
	}
	
	private static readonly Queue<IEnumerator> s_enumerators = new Queue<IEnumerator>();
	private static readonly Queue<IEnumerator> s_stopEnumerators = new Queue<IEnumerator>();

	private static UnityMainThread s_unityMainThread = null;
	private MonoBehaviour m_monoBehaviour = null;
}