﻿using System.IO;
using TMGCore;
using UnityEngine;

public class SingletonGameObjectManualLoader<T> : SingletonGameObjectManual<T>
	where T : SingletonGameObjectManualLoader<T>
{
	protected override string PrefixName { get { return "SingletonGameObjectManualLoader_"; } }
	protected override Transform Root { get { return SingletonUtils.ManualLoaderRoot; } }

	private const string ResourceFolderPath = "LoadingPrefabs";

	public new static void InitInstance()
	{
		Assert.TestStrong(s_instance == null, "InitInstance '" + typeof(T).Name + "' but instance already exist");

		string path = Path.Combine(ResourceFolderPath, typeof(T).Name);
		T loaded = (T)Resources.Load(path, typeof(T));
		Assert.TestStrong(loaded != null, "Prefab not found for singleton game object manual loader '" +
			typeof(T).Name + "' at path '" + path + "'");
		Instance = Instantiate(loaded);
	}
}