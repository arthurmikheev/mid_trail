using TMGCore;
using UnityEngine;

public class SingletonGameObjectManual<T> : MonoBehaviour
	where T: SingletonGameObjectManual<T>
{
	public static T Instance
	{
		get { return s_instance; }
		protected set
		{
			if (s_instance == value)
			{
				return;
			}

			if (s_instance != null)
			{
				s_instance.DeInit();
			}
			s_instance = value;
			if (value != null)
			{
				value.name = value.PrefixName + typeof(T).Name;
#if UNITY_EDITOR
				if(UnityEditor.EditorApplication.isPlaying)
#endif
				{
					DontDestroyOnLoad(value);
					value.transform.SetParent(value.Root);
				}
				value.Init();
			}
		}
	}

	protected virtual string PrefixName { get { return "SingletonGameObjectManual_"; } }
	protected virtual Transform Root { get { return SingletonUtils.ManualRoot; } } 
	
	protected static T s_instance = null;

	public static T InitInstance()
	{
		Assert.TestStrong(s_instance == null, "InitInstance '" + typeof(T).Name + "' but instance already exist");

		GameObject gigo = new GameObject();
		Instance = gigo.AddComponent<T>();
		return Instance;
	}

	public static void FreeInstance()
	{
#if UNITY_EDITOR
		if (!UnityEditor.EditorApplication.isPlaying)
		{
			DestroyImmediate(Instance.gameObject);
		}
		else
#endif
		{
			Destroy(Instance.gameObject);
		}
		Instance = null;
	}

	protected virtual void Init()
	{
	}

	protected virtual void DeInit()
	{
	}
}