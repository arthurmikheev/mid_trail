﻿using UnityEngine;
using Logger = TMGCore.Logger;

public class SingletonGameObject<T> : MonoBehaviour
	where T : SingletonGameObject<T>
{
    public static T Instance
    {
        get
        {
            if (s_instance == null)
            {
                var holder = new GameObject(PrefixName + typeof(T).ToString());
                s_instance = holder.AddComponent<T>();
				s_instance.transform.SetParent(SingletonUtils.SingletonRoot);
				s_instance.Init();
            }
            return s_instance;
        }
    }

	public static T TryInstance
	{
		get { return s_instance; }
	}

	private const string PrefixName = "SingletonGameObject_";

	private static T s_instance = null;

	public static void InitInstance()
	{
		T isntance = Instance;
		if (isntance == null)
		{
			Logger.LogWarning("'" + typeof(T).Name + "' instance is null");
		}
	}

	public static void FreeInstance()
	{
		s_instance.DeInit();
		Destroy(s_instance.gameObject);
		s_instance = null;
	}

	protected virtual void Init()
	{
	}

	protected virtual void DeInit()
	{
	}
}
