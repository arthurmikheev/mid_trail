﻿using UnityEngine;

public static class SingletonUtils
{
	public static Transform SingletonRoot
	{
		get
		{
			if (s_singletonRoot == null)
			{
				GameObject rootgo = new GameObject(SingletonRootName);
				Object.DontDestroyOnLoad(rootgo);
				s_singletonRoot = rootgo.transform;
			}
			return s_singletonRoot;
		}
	}

	public static Transform ManualRoot
	{
		get
		{
			if (s_manualRoot == null)
			{
				GameObject rootgo = new GameObject(ManualRootName);
				Object.DontDestroyOnLoad(rootgo);
				rootgo.transform.SetParent(SingletonRoot);
				s_manualRoot = rootgo.transform;
			}
			return s_manualRoot;
		}
	}

	public static Transform ManualLoaderRoot
	{
		get
		{
			if (s_manualLoaderRoot == null)
			{
				GameObject rootgo = new GameObject(ManualLoaderRootName);
				Object.DontDestroyOnLoad(rootgo);
				rootgo.transform.SetParent(SingletonRoot);
				s_manualLoaderRoot = rootgo.transform;
			}
			return s_manualLoaderRoot;
		}
	}

	private const string SingletonRootName = "Singletons";
	private const string ManualRootName = "Manuals";
	private const string ManualLoaderRootName = "ManualLoaders";

	private static Transform s_singletonRoot = null;
	private static Transform s_manualRoot = null;
	private static Transform s_manualLoaderRoot = null;
}